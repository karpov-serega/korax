<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;

\app\assets\AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    if (!Yii::$app->user->isGuest) {
        NavBar::begin([
            'brandLabel' => Html::img('@web/img/gerb.png', ['alt' => Yii::$app->name, 'style' => 'height: 45px; margin-top: -12px;']),
            'brandUrl' => Yii::$app->homeUrl,
            'options' => [
                'class' => 'navbar-inverse navbar-fixed-top',
            ],
        ]);
            $menuItems[] = ['label' => 'Акции', 'url' => ['/ipra/index']];
            $menuItems[] = ['label' => 'Каталог', 'url' => ['/site/catalog']];
            $menuItems[] = ['label' => 'Категории', 'url' => ['/patient/index']];
//            $menuItems[] = ['label' => 'Отчеты', 'url' => ['/reports']];
            Yii::$app->user->isGuest ? $menuItems[] = ['label' => 'Login', 'url' => ['/login']] :
            $menuItems[] = [
                'label' => 'Выход (' . Yii::$app->user->identity->username . ')',
                'url' => ['/site/logout'],
                'linkOptions' => ['data-method' => 'post']
            ];

        echo Nav::widget([
            'options' => ['class' => 'navbar-nav navbar-right'],
            'items' => $menuItems,
        ]);
        NavBar::end();
    }
    ?>

    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= \kartik\widgets\Alert::widget() ?>
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">
            &copy; <?= Html::a("ГБУЗ СК МИАЦ", \yii\helpers\Url::to('http://skmiac.ru/index.php?id=269'), ["target" => "_blank"]); ?> <?= date('Y') ?>
<!--            &copy; ГБУЗ СК МИАЦ --><?//= date('Y') ?>
        </p>

        <!--        <p class="pull-right">--><?php //= Yii::powered() ?><!--</p>-->
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
