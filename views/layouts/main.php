<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>

    <meta name="description" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!--<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">-->
    <meta name="viewport" content="width=1024px, minimum-scale=0.25, maximum-scale=1.0, user-scalable=yes">
    <link rel="shortcut icon" href="img/favicon/favicon.ico" type="image/x-icon">
    <link rel="apple-touch-icon" href="img/favicon/apple-touch-icon.png">
    <link rel="apple-touch-icon" sizes="72x72" href="img/favicon/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="114x114" href="img/favicon/apple-touch-icon-114x114.png">
    <link rel="stylesheet" href="">


    <meta charset="<?= Yii::$app->charset ?>">
<!--    <meta name="viewport" content="width=device-width, initial-scale=1">-->
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body style="
    background-image: url(../../img/top_1.png), url(../../img/bottom_1.png);
    background-position: top, bottom;
    background-repeat: no-repeat;
    position: relative;
">
<?php $this->beginBody() ?>


<header>
    <div class="top-line">
        <div class="container">
            <div class="row">
                <div class="col-md-2 col-sm-6 col-xs-2">
                    <a href="/" class="logo"><?= Html::img('@web/img/logo.png', ['alt'=>'korax rostov']); ?></a>
                </div>

                <div class="col-lg-2 col-md-9 col-sm-6 col-xs-9 col-lg-push-7">
                    <div class="phone-wrap">
                        <div class="phone">
                            +7 (863) 279-91-63
                        </div>
                        <a href="#" class="toggle-mnu hidden-lg"><span></span></a>
                    </div>
                </div>

                <div class="col-lg-6 col-md-12 col-lg-pull-2">
                    <nav class="main-mnu">
                        <ul>
                            <li><a href="<?= Yii::$app->homeUrl; ?>#actions_block" class="a_menu">Акции</a></li>
                            <li><a href="<?= Yii::$app->homeUrl; ?>#catalog_block" class="a_menu">Каталог</a></li>
                            <li><a href="<?= Yii::$app->homeUrl; ?>#uslugi_block" class="a_menu">Услуги</a></li>
                            <li><a href="<?= Yii::$app->homeUrl; ?>#contacts_block" class="a_menu">Контакты</a></li>
                        </ul>
                    </nav>
                </div>


            </div>
        </div>
    </div>

    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= $content ?>
    </div>

    <div class="footer">
        <div class="container">
            <div class="col-md-2 col-sm-6 col-xs-2 copyright">
                2015, ООО “Коракс”
            </div>

            <div class="col-md-4 authors">
                <span>Дизайн: Гребенюк Алена</span>

                <span>Программирование и верстка: Карпов Сергей</span>
            </div>


        </div>
    </div>

</header>
<!--<div class="main_head" style="background-image: url(img/bottom_1.png); background-position: bottom; background-repeat: no-repeat;"></div>-->
<div class="hidden"></div>
<!--[if lt IE 9]
<script src="libs/html5shiv/es5-shim.min.js"></script
<script src="libs/html5shiv/html5shiv.min.js"></script
<script src="libs/html5shiv/html5shiv-printshiv.min.js"></script
<script src="libs/respond/respond.min.js"></script
<![endif]-->



<!— Yandex.Metrika counter —>
<script type="text/javascript">
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter35637095 = new Ya.Metrika({
                    id:35637095,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true
                });
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/35637095" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!— /Yandex.Metrika counter —>





<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
