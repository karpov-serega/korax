<?php

use \yii\helpers\Url;
use \app\models\Products;
use \yii\helpers\Html;
use yii\widgets\Pjax;

/* @var $this yii\web\View */

$this->title = 'Коракс. Надежный партнер';
?>
<div class="container content">
    <div class="content_main">
        <div class="header-slogan">
            Продажа и адаптация<br/> программных продуктов <span style="color: #f2bf00;">1С</span> <br/>в
            Ростовской Области
        </div>

        <div class="actions" id="actions_block">
            <span class="title">Акции</span>

            <div class="left">
                <div class="logo"></div>
                <span>Бесплатная установка</span>
                При покупке любой конфигурации 1С версии ПРОФ наши специалисты приедут, бесплатно установят и
                запустят в
                эксплуатацию базу данных (установка, настройка базы и настройка резервного копирования).
            </div>
            <div class="right">
                <div class="logo"></div>
                <span>Бесплатная помощь</span>
                Специалист нашей компании подъедет к Вам в офис в удобное время, проведет тестирование базы данных,
                ответит на любой вопрос по ведению учета в 1С, а также обучит ваших сотрудников пользованию системой
                1С
                Информационно - Технологическое Сопровождение (ИТС).
            </div>
        </div>

        <div class="actions" id="catalog_block">
            <span class="title">Каталог</span>
            <?php Pjax::begin(); ?>
            <div class="col-lg-12 col-md-12">
                <div class="cat-mnu">
                    <ul>
                        <li>
                            <?= Html::a('Все продукты', ['site/index', 'all' => true]) ?>
                        </li>
                        <? $categories = \app\models\Category::find()->all();?>
                        <? foreach ($categories as $category) : ?>
                        <li>
                            <?= Html::a($category->translation, ['site/index', 'cat' => $category->id]) ?>
                        </li>
                        <? endforeach; ?>
                    </ul>
                </div>
            </div>
            <div class="cat-list">
                <? foreach ($products as $product) : ?>
                <div class="cat-product" onclick="location.href = '<?= Url::to(['site/product', 'id' => $product->id]);?>'">
                    <div class="cat"><?= $r = $product->categoryValue; ?></div>
                    <div class="img"><?= Html::img('@web/img/catalog/'.$product->photo, ['alt'=>Yii::$app->name]); ?></div>
                    <div class="name"><?= $product->name; ?></div>
                    <div class="price"><?= $product->price; ?> &#8381;</div>
                </div>
                <? endforeach; ?>
            </div>
            <?php Pjax::end(); ?>
        </div>

        <div class="actions" id="uslugi_block">
            <span class="title">Услуги</span>

            <div class="service">
                <div class="logo"
                     style="background-image: url(../../img/SystemReport.png); background-repeat: no-repeat; background-position: center; position: relative"></div>
                <span>Информационно-технологическое сопровождение 1с</span>
                Вся линейка программных продуктов 1С постоянно обновляется и модифицируется. Будьте в курсе всех
                возможностей своей версии! Воспользуйтесь нашими услугами, чтобы установленные программы всегда
                отвечали
                необходимым требованиям и позволяли использовать новые функции.
            </div>
            <div class="service">
                <div class="logo"
                     style="background-image: url(../../img/OnlineSupport.png); background-repeat: no-repeat; background-position: center; position: relative"></div>
                <span>Консультации</span>
                Обратившись к специалистам нашей бесплатной линии консультаций, Вы всегда получите быстрый и
                квалифицированный ответ. Сотрудники компании в любой момент готовы выехать к Вам в офис для решения
                самых неотложных вопросов, требующих внимания профессионала!
            </div>
            <div class="service">
                <div class="logo"
                     style="background-image: url(../../img/Support.png); background-repeat: no-repeat; background-position: center; position: relative"></div>
                <span>Настройка и доработка программных продуктов 1с</span>
                Работа каждой фирмы имеет свою специфику. Наши специалисты готовы настроить Вашу программу так,
                чтобы
                она учитывала все Ваши потребности на 100%. Мы работаем с любыми конфигурациями и выполняем любые
                работы
                по настройке и доработке программных продуктов 1С.
            </div>
        </div>

        <div class="actions" style="margin-bottom: 70px;" id="contacts_block">
            <span class="title">Контакты</span>

            <div class="content">
                <div class="col-md-3">
                    <div class="img_contacts">
                        <?= Html::img('@web/img/Message.png', ['alt'=>Yii::$app->name]); ?>
                    </div>
                    support@koraks-rostov.ru<br/>
                    director@koraks-rostov.ru<br/>
                    manager@koraks-rostov.ru
                </div>
                <div class="col-md-3">
                    <div class="img_contacts">
                        <?= Html::img('@web/img/MapMarker.png', ['alt'=>Yii::$app->name]); ?>
                    </div>
                    г. Ростов-на-Дону<br/>
                    ул. Социалистическая 88,<br/>
                    офис 606
                </div>
                <div class="col-md-3">
                    <div class="img_contacts">
                        <?= Html::img('@web/img/Time.png', ['alt'=>Yii::$app->name]); ?>
                    </div>
                    Пн-пт — 9:00-18:00,<br/>
                    Сб, вс — выходной
                </div>
                <div class="col-md-3">
                    <div class="img_contacts">
                        <?= Html::img('@web/img/Phone.png', ['alt'=>Yii::$app->name]); ?>
                    </div>
                    +7 (863) 279-91-63
                </div>
            </div>
        </div>
    </div>
</div>

<script>

</script>
