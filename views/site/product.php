<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Products */

$this->title = $model->name;
?>
<div class="products-view">

    <div class="container content">
        <div class="content_product">
            <div class="row">
                <div class="col-md-12">
                    <div class="cat"><?= $model->categoryValue; ?></div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <?= Html::img('@web/img/catalog/'.$model->photo, ['alt'=>Yii::$app->name, "class" => "img-product"]); ?>
                    <div class="question_block">
                        <span class="head">Есть вопросы?</span>
                        <span class="text">Мы всегда с радостью проконсультируем вас!</span>
                        <span class="phone">+7 (863) 279-91-63</span>
                    </div>
                </div>
                <div class="col-md-8" style="font-family: PTSans;">
                    <h3>
                        <?= $model->name; ?>
                    </h3>
                    <div class="price"><?= $model->price; ?> &#8381;</div>
                    <p class="description_product">
                        <?= nl2br($model->description); ?>
<!--                        "1С:Управление торговлей 8" автоматизирует следующие направления хозяйственной деятельности:-->
                    </p>
<!--                    <ul>-->
<!--                        <li>- управление отношениями с клиентами,</li>-->
<!--                        <li>- управление правилами продаж,</li>-->
<!--                        <li>- управление процессами продаж:</li>-->
<!--                        <li>- управление торговыми представителями,</li>-->
<!--                        <li>- управление запасами,</li>-->
<!--                        <li>- управление закупками,</li>-->
<!--                        <li>- управление складом,</li>-->
<!--                        <li>- управление доставкой товаров,</li>-->
<!--                        <li>- управление финансами,</li>-->
<!--                        <li>- контроль и анализ целевых показателей деятельности предприятия.</li>-->
<!--                    </ul>-->
                </div>
            </div>
            <div class="actions" style="margin-bottom: 70px;">
                <span class="title">Контакты для заказа продукта</span>

                <div class="content">
                    <div class="col-md-3">
                        <div class="img_contacts">
                            <?= Html::img('@web/img/Message-gray.png', ['alt'=>Yii::$app->name]); ?>
                        </div>
                        support@koraks-rostov.ru<br/>
                        director@koraks-rostov.ru<br/>
                        manager@koraks-rostov.ru
                    </div>
                    <div class="col-md-3">
                        <div class="img_contacts">
                            <?= Html::img('@web/img/Map-gray.png', ['alt'=>Yii::$app->name]); ?>
                        </div>
                        г. Ростов-на-Дону<br/>
                        ул. Социалистическая 88,<br/>
                        офис 606
                    </div>
                    <div class="col-md-3">
                        <div class="img_contacts">
                            <?= Html::img('@web/img/Time-gray.png', ['alt'=>Yii::$app->name]); ?>
                        </div>
                        Пн-пт — 9:00-18:00,<br/>
                        Сб, вс — выходной
                    </div>
                    <div class="col-md-3">
                        <div class="img_contacts">
                            <?= Html::img('@web/img/Phone-gray.png', ['alt'=>Yii::$app->name]); ?>
                        </div>
                        +7 (863) 279-91-63
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
