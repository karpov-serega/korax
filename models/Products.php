<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "Products".
 *
 * @property integer $id
 * @property string $name
 * @property string $description
 * @property string $photo
 * @property integer $price
 * @property integer $category
 * @property string $main
 */
class Products extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Products';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'description', 'photo', 'price', 'category', 'main'], 'required'],
            [['name', 'description', 'main'], 'string'],
            [['price', 'category'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'description' => 'Description',
            'photo' => 'Photo',
            'price' => 'Price',
            'category' => 'Category',
            'main' => 'Main',
        ];
    }

    public function getCategories()
    {
        return $this->hasOne(Category::className(), ['id' => 'category']);
    }

    public function getCategoryValue()
    {
        return $this->categories->translation;
    }
}
