<?php

namespace app\controllers;

use app\models\User;
use Yii;
use app\models\Products;
use app\models\ProductsSearch;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\UploadForm;
use yii\web\UploadedFile;

/**
 * ProductsController implements the CRUD actions for Products model.
 */
class ProductsController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Products models.
     * @return mixed
     */
    public function actionIndex()
    {
        if (User::findIdentity(Yii::$app->user->id) == null){
            return $this->redirect(Url::toRoute('site/login'));
        }
        $searchModel = new ProductsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Products model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        if (User::findIdentity(Yii::$app->user->id) == null){
            return $this->redirect(Url::toRoute('site/login'));
        }
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Products model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        if (User::findIdentity(Yii::$app->user->id) == null){
            return $this->redirect(Url::toRoute('site/login'));
        }
        $model = new Products();

        $modelUp = new UploadForm();

        if (Yii::$app->request->isPost) {
            $modelUp->imgFile = UploadedFile::getInstance($modelUp, 'imgFile');
            if ($modelUp->upload()) {
                $filename = $modelUp->imgFile->name;
                if (file_exists('img/catalog/' . $filename)) {
                    $text = file_get_contents('img/catalog/' . $filename);
                    $needles = array('ct:', 'false', 'true');
                    $replacements = array('', 0, 1);
                    $result = str_replace($needles, $replacements, $text);
                    file_put_contents('img/catalog/' . $filename, $result, LOCK_EX);
                    $xml = simplexml_load_file('img/catalog/' . $filename);
                }
            }
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Products model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public
    function actionUpdate($id)
    {
        if (User::findIdentity(Yii::$app->user->id) == null){
            return $this->redirect(Url::toRoute('site/login'));
        }
        $model = $this->findModel($id);

        $modelUp = new UploadForm();

        if (Yii::$app->request->isPost) {
            $modelUp->photo = UploadedFile::getInstance($modelUp, 'photo');
            if ($modelUp->upload()) {
                $filename = $modelUp->photo->name;
                $model->photo = $filename;
                $model->save();
            }
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'modelUp' => $modelUp,
            ]);
        }
    }

    /**
     * Deletes an existing Products model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public
    function actionDelete($id)
    {
        if (User::findIdentity(Yii::$app->user->id) == null){
            return $this->redirect(Url::toRoute('site/login'));
        }
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Products model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Products the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected
    function findModel($id)
    {
        if (($model = Products::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
