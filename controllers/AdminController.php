<?php

namespace app\controllers;

use yii\web\Controller;

/**
 * NewsController implements the CRUD actions for News model.
 */
class AdminController extends Controller
{

    public $layout = 'admin';

    public function actionIndex()
    {
//        $this->redirect('/admin');
        return $this->render('index', [
        ]);
    }

}
